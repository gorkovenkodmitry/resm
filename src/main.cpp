
#include <unistd.h>
#include <iostream>
#include <map>
#include <sstream>
#include <cstdlib>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string.h>
#define MAXBUF  10000
char buffer[MAXBUF];
#define MAXPATH 255
#include <fstream>
#include "resm.h"
#include "configuration.h"


std::string get_responce(const char *status_code, const char *content_type, const std::string content) {
    std::ostringstream response;
    response << "HTTP/1.1 " << status_code << "\x0D\x0A";
    response << "Server: resm\x0D\x0A";
    response << "Content-Type: " << content_type << "; charset=utf-8\x0D\x0A";
    response << "Connection: close";
    response << "\x0D\x0A";
    response << "\x0D\x0A";
    response << content;
    return response.str();
}


int main(int argc, char* argv[])
{
    configuration::data config_data;
    if (argc > 1) {
        config_data = configuration::config_parse(argv[1]);
    }
    int port = (config_data.count("port")==1)?std::atoi(config_data.at("port").c_str()):10000;
    struct sockaddr_in addr;
    int sd, addrlen = sizeof(addr);
    if ( (sd = socket(PF_INET, SOCK_STREAM, 0)) < 0 )
        perror("Socket");
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    if ( bind(sd, (struct sockaddr*)&addr, addrlen) < 0 ) {
        perror("Bind");
        return 0;
    }
    if ( listen(sd, 5) < 0 )
        perror("Listen");
    ResourceManager *resm = new ResourceManager((config_data.count("resource_count")==1)?std::atoi(config_data.at("resource_count").c_str()):3);
    while (1)
    {
        int len;
        int client = accept(sd, NULL, NULL);
        if ( (len = recv(client, buffer, MAXBUF, 0)) > 0 )
        {
            FILE* ClientFP = fdopen(client, "w");
            if ( ClientFP == NULL )
                perror("fpopen");
            else
            {
                // Получить тип запроса
                char Req[MAXPATH];
                sscanf(buffer, "%s /", Req);
                // Если тип запроса - POST
                if (strcmp(Req, "POST") == 0)
                {
                    // Обработка пути
                    char path[MAXPATH];
                    char dirpath[MAXPATH];
                    sscanf(buffer, "POST %s HTTP", path);
                    getcwd(dirpath, MAXPATH);
                    std::string response = get_responce("400  Bad Request", "application/json", "\"Bad Request\"");
                    fprintf(ClientFP, response.c_str());
                }
                // Если тип запроса - GET
                else if (strcmp(Req, "GET") == 0)
                {
                    char path[MAXPATH], name[MAXPATH];
                    sscanf(buffer, "GET %s HTTP", path);
                    std::string path_c(path);
                    std::string response;
                    if (path_c.find("/allocate/") != std::string::npos) {
                        sscanf(path, "/allocate/%s", name);
                        std::string resm_status = resm->allocate(name);
                        if (resm_status != "") {
                            response = get_responce("201  Created", "application/json", "\""+resm_status+"\"");
                        } else {
                            response = get_responce("503  Service Unavailable", "application/json", "\"Out of resources.\"");
                        }
                    } else if (path_c.find("/deallocate/") != std::string::npos) {
                        sscanf(path, "/deallocate/%s", name);
                        if (resm->deallocate(name) == 1) {
                            response = get_responce("204  No Content", "application/json", "");
                        } else {
                            response = get_responce("404  Not Found", "application/json", "\"Not allocated.\"");
                        }
                    } else if (strcmp(path, "/list") == 0) {
                        response = get_responce("200  Ok", "application/json", resm->str());
                    } else if (path_c.find("/list/") == 0) {
                        sscanf(path, "/list/%s", name);
                        response = get_responce("200  Ok", "application/json", resm->str(name));
                    } else if (strcmp(path, "/reset") == 0) {
                        resm->reset();
                        response = get_responce("204  Ok", "application/json", " ");
                    } else {
                        response = get_responce("400  Bad Request", "application/json", "\"Bad Request\"");
                    }
                    fprintf(ClientFP, response.c_str());
                }
            fclose(ClientFP);
            }
        }
        close(client);
    }
    return 0;
}
