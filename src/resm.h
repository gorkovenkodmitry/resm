#ifndef __RESM_H__
#define __RESM_H__

#include <iostream>
#include <vector>
#include <string>

#pragma pack(push,1)
struct Resource {
    std::string rname;
    std::string name;

    Resource(std::string v1, std::string v2): rname(v1), name(v2) {
    }
};
#pragma pack(pop)


class ResourceManager {
private:
    std::vector<Resource> resources_;
    std::vector<int> allocate_;
    std::vector<int> deallocated_;

public:
    ResourceManager(const int var);
    ~ResourceManager();
    int get_resource_count();
    int get_allocate_count();
    Resource resource(int i);
    std::string allocate(std::string rname);
    int deallocate(std::string name);
    void reset();
    void print();
    std::string str();
    std::string str(std::string name);
};

#endif //__RESM_H__