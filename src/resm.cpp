#include <iostream>
#include <vector>
#include <sstream>
// #include <string>
#include "resm.h"

ResourceManager::ResourceManager(const int var): resources_(), allocate_(), deallocated_() {
    for (int i=0; i<var; i++) {
        std::ostringstream rname;
        rname << "r" << (i + 1);
        resources_.push_back(Resource(rname.str(), ""));
        deallocated_.push_back(i);
    }
}

ResourceManager::~ResourceManager() {
}

int ResourceManager::get_resource_count() {
    return (int)resources_.size();
}

int ResourceManager::get_allocate_count() {
    return (int)allocate_.size();
}

Resource ResourceManager::resource(int i) {
    return resources_[i];
}

std::string ResourceManager::allocate(std::string name) {
    // 0 если ошибка, 1 если успешно
    if (deallocated_.size() == 0) {
        return "";
    }
    int ind = deallocated_[0];
    deallocated_.erase(deallocated_.begin());
    resources_[ind].name = name;
    allocate_.push_back(ind);
    return resources_[ind].rname;
}

int ResourceManager::deallocate(std::string rname) {
    // 0 если ошибка, 1 если успешно
    int allocate_size = (int)allocate_.size();
    if (allocate_size == 0) {
        return 0;
    }
    int ind = -1;
    for (int i=0; i<allocate_size; i++) {
        if (resources_[allocate_[i]].rname == rname) {
            ind = i;
            break;
        }
    }
    if (ind == -1) {
        return 0;
    }
    resources_[allocate_[ind]].name = "";
    deallocated_.push_back(allocate_[ind]);
    allocate_.erase(allocate_.begin()+ind);
    return 1;
}

void ResourceManager::reset() {
    allocate_.clear();
    deallocated_.clear();
    for (int i=0; i<(int)resources_.size(); i++) {
        resources_[i].name = "";
        deallocated_.push_back(i);
    }
}

void ResourceManager::print() {
    std::cout << str() << std::endl;
}

std::string ResourceManager::str() {
    std::ostringstream result;
    result << "{";
    int allocate_size = (int)allocate_.size();
    if (allocate_size > 0) {
        result << "\"allocate\":{";
        for (int i=0; i<allocate_size; i++) {
            if (i == allocate_size - 1) {
                result << "\"" << resources_[allocate_[i]].rname << "\":\"" << resources_[allocate_[i]].name << "\"";
            } else {
                result << "\"" << resources_[allocate_[i]].rname << "\":\"" << resources_[allocate_[i]].name << "\",";
            }
        }
        result << "}";
    } else {
        result << "\"allocate\":[]";
    }
    int deallocated_size = (int)deallocated_.size();
    if (deallocated_size > 0 ) {
        result << ",\"deallocated\":[";
        for (int i=0; i<deallocated_size; i++) {
            if (i == deallocated_size - 1) {
                result << "\"" << resources_[deallocated_[i]].rname << "\"";    
            } else {
                result << "\"" << resources_[deallocated_[i]].rname << "\",";
            }
            
        }
        result << "]}";
    } else {
        result << ",\"deallocated\":[]}";
    }
    return result.str();
}

std::string ResourceManager::str(std::string name) {
    std::ostringstream result;
    result << "[";
    int allocate_size = (int)allocate_.size();
    int count = 0;
    for (int i=0; i<allocate_size; i++) {
        if (resources_[allocate_[i]].name == name) {
            if (count > 0) {
                result << ",";
            }
            result << "\"" << resources_[allocate_[i]].rname << "\"";
            count++;
        }
    }
    result << "]";
    return result.str();
}
