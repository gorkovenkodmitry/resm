SYSCONF_LINK := g++
CPPFLAGS :=
LDFLAGS :=
LIBS := -lm -lboost_unit_test_framework
CFLAGS := -g
INC := -I include

BUILDDIR := build/
TARGET := $(BUILDDIR)resm
TEST_TARGET := $(BUILDDIR)tester
SRCDIR := src/
SRCEXT := cpp

SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))

all: clean $(TARGET) $(TEST_TARGET)

$(TARGET): $(OBJECTS)
	@mkdir -p $(BUILDDIR)
	$(SYSCONF_LINK) $(CFLAGS) $(LDFLAGS) -o $(TARGET) $(OBJECTS) $(LIBS)

$(OBJECTS): %.o: %.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	$(SYSCONF_LINK) $(CFLAGS) $(CPPFLAGS) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	@echo " $(SYSCONF_LINK) $(CFLAGS) $(INC) -c -o $@ $<"; $(SYSCONF_LINK) $(CFLAGS) $(INC) -c -o $@ $<

# Tests
$(TEST_TARGET):
	$(SYSCONF_LINK) $(CFLAGS) test/tester.cpp src/resm.cpp $(INC) $(LIBS) -o $(TEST_TARGET)

clean:
	@echo " Cleaning..."; 
	@echo " $(RM) -r $(BUILDDIR) $(TARGET)"; $(RM) -r $(BUILDDIR) $(TARGET)
	rm -f $(TARGET)
	rm -f $(TEST_TARGET)

test: $(TEST_TARGET)
	@echo "Testing"
	$(TEST_TARGET)

run:
	@echo "Running"
	$(TEST_TARGET)
	cp $(SRCDIR)config.ini $(BUILDDIR)config.ini
	$(TARGET) $(BUILDDIR)config.ini
