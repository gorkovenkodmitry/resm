# resm - Resource manager. #
Resm is a simple resource manager with REST API interface and JSON data format

# Requirements #
g++, libevent-dev, libboost-test-dev

# Installation #
## From sources ##
Clone repository:

```
#!bash

git clone git@bitbucket.org:gorkovenkodmitry/resm.git
```

Build:

```
#!bash

make
```

# Run #

```
#!bash

make run
```

# You can run unit tests #

```
#!bash

make test
```

# From docker image #
```
#!bash

docker build -t gorkovenkodmitry/resm ./
docker run -t -i -p 10000:10000 gorkovenkodmitry/resm /bin/resm
```

# Configuration #
You can change the settings in the file build/config.ini

# To check out how it works, go to [http://127.0.0.1:10000/list](http://127.0.0.1:10000/list) #

#REST API


## Allocate ##

1.

● {allocated:{},deallocated:[r1,r2,r3]}

● GET allocate/alice HTTP/1.1

● 201 Created

● r1

2.

● {allocated:{r1:alice,r2:bob,r3:alice},deallocated:[]}

● GET allocate/bob

● 503 Service Unavailable HTTP/1.1

● Out of resources.

## Deallocate ##

1.

● {allocated:{r2:bob,r3:alice},deallocated:[r1]}

● GET deallocate/r2 HTTP/1.1

● 204 No Content

2.

● {allocated:{r3:alice},deallocated:[r1,r2]}

● GET deallocate/r1 HTTP/1.1

● 404 Not Found

● Not allocated.

3.

● {allocated:{r3:alice},deallocated:[r1,r2]}

● GET deallocate/any HTTP/1.1

● 404 Not Found

● Not allocated.

## List ##

1.

● {allocated:[],deallocated:[r1,r2,r3]}

● GET list HTTP/1.1

● 200 OK

● {"allocated":[],"deallocated":["r1","r2","r3"]}

2.

● {allocated:{r1:alice,r2:bob},deallocated:[r3]}

● GET list HTTP/1.1

● 200 OK

● {"allocated":{"r1":"alice","r2":"bob"},"deallocated":["r3"]}

3.

● {allocated:{r1:alice,r2:bob,r3:alice},deallocated:[]}

● GET list/alice HTTP/1.1

● 200 OK

● ["r1","r3"]

4.

● {allocated:{r1:alice,r3:alice},deallocated:[r2]}

● GET list/bob HTTP/1.1

● 200 OK

● []

## Reset ##

1.

● {allocated:{r1:alice,r2:bob,r3:alice},deallocated:[]}

● GET reset HTTP/1.1

● 204 No Content

## Bad request ##

1.

● [Any state]

● [Any HTTP method and path other than described above]

● 400 Bad Request

● Bad request.