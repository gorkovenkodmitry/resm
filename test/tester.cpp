#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Fixtures
#include "../src/resm.h"
#include <boost/test/unit_test.hpp>

struct FixtureStruct
{
    FixtureStruct(): resm(3) {
    }
 
    ~FixtureStruct() {
    }
 
    ResourceManager resm;
};

BOOST_FIXTURE_TEST_SUITE(Resm, FixtureStruct)

BOOST_AUTO_TEST_CASE(get_resource_count)
{
    BOOST_CHECK_EQUAL(resm.get_resource_count(), 3);
}

BOOST_AUTO_TEST_CASE(allocate)
{
    BOOST_CHECK_EQUAL(resm.allocate("alice"), "r1");
    resm.allocate("bob");
    resm.allocate("alice");
    BOOST_CHECK_EQUAL(resm.allocate("bob"), "");
}

BOOST_AUTO_TEST_CASE(deallocate)
{
    resm.allocate("alice");
    resm.allocate("bob");
    resm.allocate("alice");
    resm.deallocate("r1");
    BOOST_CHECK_EQUAL(resm.deallocate("r2"), 1);
    BOOST_CHECK_EQUAL(resm.deallocate("r1"), 0);
}

BOOST_AUTO_TEST_CASE(list)
{
    BOOST_CHECK_EQUAL(resm.str(), "{\"allocate\":[],\"deallocated\":[\"r1\",\"r2\",\"r3\"]}");
    resm.allocate("alice");
    resm.allocate("bob");
    BOOST_CHECK_EQUAL(resm.str(), "{\"allocate\":{\"r1\":\"alice\",\"r2\":\"bob\"},\"deallocated\":[\"r3\"]}");
    resm.allocate("alice");
    BOOST_CHECK_EQUAL(resm.str("alice"), "[\"r1\",\"r3\"]");
    resm.deallocate("r2");
    BOOST_CHECK_EQUAL(resm.str("bob"), "[]");
}

BOOST_AUTO_TEST_CASE(reset)
{
    resm.allocate("alice");
    resm.allocate("bob");
    resm.allocate("alice");
    BOOST_CHECK_EQUAL(resm.str(), "{\"allocate\":{\"r1\":\"alice\",\"r2\":\"bob\",\"r3\":\"alice\"},\"deallocated\":[]}");
}

BOOST_AUTO_TEST_SUITE_END()