FROM ubuntu:14.04
MAINTAINER Gorkovenko Dmitry <gorkovenko.dmitry@gmail.com>
RUN apt-get update
RUN apt-get -y install wget make g++ libevent-dev libboost-test-dev unzip
RUN cd /home && wget https://bitbucket.org/gorkovenkodmitry/resm/get/master.zip
RUN cd /home && unzip master.zip
RUN cd /home && mv -v gorkovenkodmitry-* resm/
RUN cd /home/resm/ && make
RUN cd /bin && ln -s /home/resm/build/resm
RUN echo 'Complete'
